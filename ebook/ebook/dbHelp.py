import pymysql


class DBHelp(object):
    def __connection__(self, host, user, pwd, database, charset):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.database = database
        self.charset = charset
        self.conn = pymysql.connect(host=self.host, user=self.user, password=self.pwd,
                                    database=self.database, charset=self.charset)

    # 打开数据库
    def open(self, host, user, pwd, database, charset):
        self.__connection__(host, user, pwd, database, charset)
        self.cursor = self.conn.cursor()

    # 增加数据
    def insert(self, sql):
        row = self.cursor.execute(sql)
        self.conn.commit()
        return row

    # 修改数据
    def update(self, sql):
        row = self.cursor.execute(sql)
        self.conn.commit()
        return row

    # 查询数据
    def select(self, sql):
        row = self.cursor.execute(sql)
        result = self.cursor.fetchone()
        return [row, result]

    # 删除数据
    def delete(self, sql):
        row = self.cursor.execute(sql)
        self.conn.commit()
        return row

    # 关闭数据库
    def close(self):
        self.cursor.close()
        self.conn.close()