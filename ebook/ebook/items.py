# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class EbookItem(scrapy.Item):
    # 书id
    ebook_id = scrapy.Field()
    # 书名
    ebook_title = scrapy.Field()
    # 作者
    ebook_author = scrapy.Field()
    # 书籍url
    ebook_url = scrapy.Field()
    # 书籍封面
    ebook_cover = scrapy.Field()
    # 类别名
    type_title = scrapy.Field()
    # 书籍标签
    ebook_tag = scrapy.Field()
    # 创建事件
    ebook_ctime = scrapy.Field()
    # ISBN
    ebook_ISBN = scrapy.Field()
    # 书籍简介
    ebook_eintro = scrapy.Field()
    # 作者简介
    ebook_aintro = scrapy.Field()


class EbookTypeItem(scrapy.Item):
    # id
    type_id = scrapy.Field()
    # 类别名
    type_title = scrapy.Field()


