from pymongo import MongoClient
client = MongoClient('mongodb://localhost:27017')
db = client.myMongo
collect = db.doubanTop

# ----- 插入数据 ------
# result = collect.insert_one({'name': '张三', 'age': 18})
# print(result.inserted_id)
# result = collect.insert_many([{'name': '李四', 'age': 22}, {'name': '王五', 'age': 25}])
# print(result.inserted_ids)

# ---- 更新数据 ------
# result = collect.update_one({'name': '张三'}, {'$set': {'name': '王大'}})
# print(result.modified_count)
# 更新多条数据
# result = collect.update_many({'age': {'$gt': 18}}, {'$set': {'name': '王大'}})
# print(result.modified_count)

#  ---- 删除数据 -----
# result = collect.delete_one({'name': '王大'})
# print(result.deleted_count)
# 删除多条数据
# result = collect.delete_many({'name': '王大'})
# print(result.deleted_count)

# ---- 查询数据 -----
# result = collect.find_one({'name': '哈哈哈'})
# print(result)
# 查询所有满足条件的数据
# for result in collect.find({}):
#     print(result)