import scrapy
import os
from sinaSpider.items import SinaspiderItem


class SinaSpider(scrapy.Spider):
    name = 'sina'
    allowed_domains = ['sina.com.cn']
    start_urls = ['http://news.sina.com.cn/guide/']

    def parse(self, response):
        """
       解析初始页面
       """
        items = []

        # 现将一级标题解析出来
        type_1_urls = response.xpath('//div[@id="tab01"]/div[@class="clearfix"][1]/h3/a/@href').extract()
        type_1_names = response.xpath('//div[@id="tab01"]/div[@class="clearfix"][1]/h3/a/text()').extract()
        # 接下来解析对应的二级标题
        type_2_urls = response.xpath('//div[@id="tab01"]/div[1]/ul/li/a/@href').extract()
        type_2_names = response.xpath('//div[@id="tab01"]/div[1]/ul/li/a/text()').extract()

        for i in range(0, len(type_1_names)):
            # 根据第一级标题来创建文件夹
            type_1_file_name = './data/' + type_1_names[i]
            if not os.path.exists(type_1_file_name):
                os.makedirs(type_1_file_name)

                # 开始处理所有的二级分类
            for j in range(0, len(type_2_names)):
                item = SinaspiderItem()
                # 保存一级分类
                item['type_1'] = type_1_names[i]

                # 判断二级分类是否是以一级分类开头的
                is_belong = type_2_urls[j].startswith(type_1_urls[i])

                # 如果是符合的子类，则可以继续处理
                if is_belong:
                    # 根据二级标题来创建文件夹
                    type_2_file_name = type_1_file_name + '/' + type_2_names[j]
                    if not os.path.exists(type_2_file_name):
                        os.makedirs(type_2_file_name)

                    # 保存item相关信息
                    item['type_2'] = type_2_names[j]
                    item['path'] = type_2_file_name
                    item['url'] = type_2_urls[j]
                    item['url_1'] = type_1_urls[i]

                    items.append(item)

            # 去请求对应的二级标题的界面
        for item in items:
            yield scrapy.Request(url=item['url'], callback=self.parse_type_2, meta={'meta_1': item})

    def parse_type_2(self, response):
        """
        解析小标题页面
        """
        # 获取对应的meta数据
        item = response.meta['meta_1']
        # 取出二级界面中所有的链接
        urls = response.xpath('//a/@href').extract()

        for i in range(0, len(urls)):
            # 判断哪些链接是属于文章的链接
            is_belong = urls[i].startswith(item['url_1']) and urls[i].endswith('.shtml')

            # 是文章的话就继续去处理
            if is_belong:
                yield scrapy.Request(url=urls[i], callback=self.parse_content, meta={'item': item})

    def parse_content(self, response):
        """
        解析文章界面
        """
        # 获取对应的meta数据
        item = response.meta['item']
        # 得到title部分
        title = response.xpath('//div[@id="article"]')
        # 解析出文章的标题
        item['name'] = title.xpath('//h1/text()')[0].extract()
        # 解析出文章的内容
        item['content'] = ''.join(title.xpath('./p/text()').extract())
        yield item