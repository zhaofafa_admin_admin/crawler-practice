# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class SinaspiderItem(scrapy.Item):
    # 文章的主分类
    type_1 = scrapy.Field()
    # 文章的二级分类
    type_2 = scrapy.Field()
    # 文章的标题
    name = scrapy.Field()
    # 文章内容
    content = scrapy.Field()
    # 文章二级分类的url
    url = scrapy.Field()
    # 文章一级分类的url
    url_1 = scrapy.Field()
    # 文章的存储路径
    path = scrapy.Field()

