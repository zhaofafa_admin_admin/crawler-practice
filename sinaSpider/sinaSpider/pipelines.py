# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter


class SinaspiderPipeline(object):
    def __init__(self):
        """
        初始化操作
        """
        pass

    def process_item(self, item, spider):
        """
        将数据写入到文件中去
        """
        with open(item['path'] + '/' + item['name'] + '.txt', 'w', encoding='utf-8') as file:
            file.write(item['content'])
        return item

    def close_spider(self, spider):
        """
        执行结束后操作
        """
        pass
