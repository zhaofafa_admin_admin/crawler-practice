# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ChapterItem(scrapy.Item):
    # 章节名称
    chapter_name = scrapy.Field()
    # 内容
    content = scrapy.Field()
    # 存储位置
    filename = scrapy.Field()


class TypeItem(scrapy.Item):
    # 类别id
    type_id = scrapy.Field()
    # 类别名称
    type_text = scrapy.Field()


class NovelItem(scrapy.Item):
    # 小说id
    novel_id = scrapy.Field()
    # 小说标题
    novel_title = scrapy.Field()
    # 小说描述
    novel_desc = scrapy.Field()
    # 小说作者
    novel_author = scrapy.Field()
    # 类别文本  ------
    type_text = scrapy.Field()
    # 封面url
    cover = scrapy.Field()


class ChapItem(scrapy.Item):
    # 章节id
    chap_id = scrapy.Field()
    # 章节标题
    chap_title = scrapy.Field()
    # 小说名 -》查数据库获得小说id  ------
    novel_title = scrapy.Field()
    # 内容url
    content_url = scrapy.Field()
    # 创建时间
    ctime = scrapy.Field()


class ContentItem(scrapy.Item):
    # 内容id
    content_id = scrapy.Field()
    # 内容
    content = scrapy.Field()
    # 章节标题  ------
    chap_title = scrapy.Field()


#  ******** 漫画*******


class ComicBookItem(scrapy.Item):
    # 书iD
    comic_id = scrapy.Field()
    # 书名
    comic_title = scrapy.Field()
    # 作者
    comic_author = scrapy.Field()
    # 封面
    comic_cover = scrapy.Field()
    # 介绍
    comic_intro = scrapy.Field()
    # 类型
    comic_type = scrapy.Field()
    # 状态
    comic_state = scrapy.Field()


class ComicChapterItem(scrapy.Item):
    # 章节ID
    comic_chap_id = scrapy.Field()
    # 章节名
    comic_chap_title = scrapy.Field()
    # 书名 -》 书id
    comic_title = scrapy.Field()


class ComicContentItem(scrapy.Item):
    # 漫画Id
    comic_con_id = scrapy.Field()
    # 漫画src
    comic_con_src = scrapy.Field()
    # 页码
    comic_con_page = scrapy.Field()
    # 章节名 -》章节id
    comic_chap_title = scrapy.Field()
    # 漫画书ID
    comic_id = scrapy.Field()


class CWMTypeItem(scrapy.Item):
    # 类别id
    type_id = scrapy.Field()
    # 类别名称
    type_title = scrapy.Field()


class CWMNovelItem(scrapy.Item):
    # 小说id
    novel_id = scrapy.Field()
    # 小说标题
    novel_title = scrapy.Field()
    # 小说描述
    novel_intro = scrapy.Field()
    # 小说作者
    novel_author = scrapy.Field()
    # 类别文本  ------
    type_title = scrapy.Field()
    # 标签
    novel_tag = scrapy.Field()
    # 状态
    novel_state = scrapy.Field()
    # 封面url
    cover = scrapy.Field()


class CWMRollItem(scrapy.Item):
    # 卷id
    roll_id = scrapy.Field()
    # 卷名
    roll_title = scrapy.Field()
    # 小说名 -》查数据库获得小说id  ------
    novel_title = scrapy.Field()


class CWMChapterItem(scrapy.Item):
    # 章节ID
    chap_id = scrapy.Field()
    # 章节名
    chap_title = scrapy.Field()
    # 书名 -》 书id
    roll_title = scrapy.Field()
    # 新增++++++++++++++++++++++
    novel_title = scrapy.Field()
    # 创建时间
    ctime = scrapy.Field()


class CWMContentItem(scrapy.Item):
    # 新增++++++++++++++++++++++
    roll_title = scrapy.Field()
    # 新增++++++++++++++++++++++
    novel_title = scrapy.Field()
    # 内容Id
    content_id = scrapy.Field()
    # 漫画src
    content_text = scrapy.Field()
    # 章节名 -》章节id
    chap_title = scrapy.Field()