import requests
import json
import execjs


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
    'Referer': 'https://www.ciweimao.com/chapter/102036462',
    'Cookie': 'ci_session=h05g9v8lvvt2hr5oacgdrqhi5fsd8s6a;'
}
response = requests.get('https://ciweimao.com/', headers=headers)
# print(response)
# print(response.text)
# print(response.cookies['ci_session'])
# cookies = dict(ci_session=response.cookies['ci_session'])
# print(cookies)

res = requests.post('https://www.ciweimao.com/chapter/ajax_get_session_code', headers=headers, data={'chapter_id': 102036462})
print(res.text)
chapter_access_key = json.loads(res.text)['chapter_access_key']
# print(dict(res.text)['chapter_access_key'])
data = {
    'chapter_id': 102036462,
    'chapter_access_key': chapter_access_key
}
print(data)
r = requests.post('https://www.ciweimao.com/chapter/get_book_chapter_detail_info', data=data, headers=headers)
print(r)
content_text = json.loads(r.text)

canshu = {
    'accessKey': chapter_access_key,
    'keys': content_text['encryt_keys'],
    'content': content_text['chapter_content'],
}

res = json.dumps(canshu)
with open('./ciweimao.js', 'r', encoding='UTF-8') as file:
    js = file.read()
do = execjs.compile(js)
print(do)

b = do.call("decrypt", res)
print(b)


