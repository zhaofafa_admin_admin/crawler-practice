# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals
import random
import base64
# 豆瓣top项目
# 从设置文件中导入所需的数据
from turorial.settings import USER_AGENTS
from turorial.settings import PROXIES


class RandomUserAgentMiddleware(object):
    """
    随机更换请求中的user-agent
    """

    def process_request(self, request, spider):
        """
        将请求头中的默认参数user-agent设置为自己从ua池中随机挑选的
        """
        user_agent = random.choice(USER_AGENTS)
        print(user_agent)
        request.headers.setdefault('User-Agent', user_agent)


class RandomProxiesMiddleware(object):
    """
    随机使用ip代理来进行访问
    """

    def process_request(self, request, spider):
        """
        在请求中使用代理来进行访问
        """
        proxy = random.choice(PROXIES)
        print(proxy)

        # 接下来需要对进行验证的ip进行处理
        if proxy['user_pwd'] is not None:
            # 有代理验证，需要进行base64对账号和密码进行加密
            base64_user_pwd = base64.b64encode(proxy['user_pwd'].encode('utf-8')).decode('utf-8')
            # 添加到对应的代理服务器请求的信令格式中,注意 Base64后面有个空格
            request.headers['Proxy-Authorization'] = 'Basic ' + base64_user_pwd

        # 进行代理ip的设置
        request.meta['proxy'] = proxy['type'] + '://' + proxy['ip_port']

# useful for handling different item types with a single interface
from itemadapter import is_item, ItemAdapter


class TurorialSpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, or item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request or item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class TurorialDownloaderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)
