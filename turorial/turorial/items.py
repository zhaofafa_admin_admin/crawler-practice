# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class DoubanspiderItem(scrapy.Item):
    # 电影名称
    name = scrapy.Field()
    # 电影评分
    score = scrapy.Field()
    # 电影简介
    info = scrapy.Field()


class TurorialItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class ItcastTeacherItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # 老师姓名
    name = scrapy.Field()
    # 老师职位
    position = scrapy.Field()
    # 老师描述信息
    info = scrapy.Field()


class DouyuspierItem(scrapy.Item):
    # 房间号
    room_id = scrapy.Field()
    # 房间名
    room_name = scrapy.Field()
    # 封面照片
    vertical_src = scrapy.Field()


class TencentcrawlspiderItem(scrapy.Item):
    # define the fields for your item here like:
    # 职位名称
    position_name = scrapy.Field()
    # 职位详情链接
    position_detail = scrapy.Field()
    # 职位类型
    position_type = scrapy.Field()
    # 工作地点
    position_addr = scrapy.Field()
    # 发布时间
    position_time = scrapy.Field()


class SuncrawlspiderItem(scrapy.Item):
    # id
    problem_id = scrapy.Field()
    # 问题标题
    problem_title = scrapy.Field()
    # 问政时间
    problem_time = scrapy.Field()

