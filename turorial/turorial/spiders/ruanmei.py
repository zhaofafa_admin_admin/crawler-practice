import scrapy
import json


class RuanmeiSpider(scrapy.Spider):
    name = 'ruanmei'
    allowed_domains = ['my.ruanmei.com']
    # 1. 进入登录界面
    start_urls = ['http://my.ruanmei.com/']

    def parse(self, response):
        # 因为不需要获取网页上的其他数据，因此可以直接发送请求
        url = 'http://my.ruanmei.com/Default.aspx/LoginUser'
        data = {
            "mail": "15355417580",
            "psw": "qwe1004515739",
            "rememberme": "true"
        }
        # 发送json请求
        yield scrapy.Request(url, method='POST', body=json.dumps(data), headers={'Content-Type': 'application/json'},
                             callback=self.parse_ruanmei)

    def parse_ruanmei(self, response):
        # 请求一个登陆之后才能看到的网页
        url = 'http://my.ruanmei.com/usercenter/base.aspx'
        yield scrapy.Request(url, callback=self.parse_page)

    def parse_page(self, response):
        # 将该网页保存下来
        with open('ruanmei.html', 'w') as filename:
            filename.write(response.text)
