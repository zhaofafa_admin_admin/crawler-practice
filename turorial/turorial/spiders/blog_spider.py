from scrapy.spiders import Spider


class BlogSpider(Spider):
    # 爬虫名称
    name = 'wangxin1248'
    # 爬虫爬取范围
    allowd_domains = ['https://wangxin1248.github.io/']
    # 爬虫真实的爬取url
    start_urls = ["https://wangxin1248.github.io"]

    # 网页响应解析
    def parse(self, response):
        titles = response.xpath('//p[@class="nice-text"]/text()').extract()
        for title in titles:
            print(title.strip())