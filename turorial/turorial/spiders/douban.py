import scrapy

from turorial.items import DoubanspiderItem


class DoubanSpider(scrapy.Spider):
    name = 'douban'
    allowed_domains = ['douban.com']
    url = 'https://movie.douban.com/top250?start='
    offset = 0
    start_urls = [url + str(offset)]

    def parse(self, response):
        movies = response.xpath('//div[@class="item"]')
        for movie in movies:
            item = DoubanspiderItem()
            try:
                # 解析网页响应
                item['name'] = movie.xpath('.//span[@class="title"][1]/text()').extract()[0]
                item['score'] = movie.xpath('.//span[@class="rating_num"]/text()').extract()[0]
                item['info'] = movie.xpath('.//span[@class="inq"]/text()').extract()[0]
                yield item
            except BaseException:
                print("ERROR")

        # 继续发送请求
        if self.offset < 225:
            self.offset += 25
        yield scrapy.Request(self.url + str(self.offset), callback=self.parse)
