# Scrapy settings for turorial project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'turorial'

# 指定日志保存路径和保存级别
# LOG_FILE = "tencent_log.log"
LOG_LEVEL = "DEBUG"

SPIDER_MODULES = ['turorial.spiders']
NEWSPIDER_MODULE = 'turorial.spiders'


# 自定义图片下载路径,名称固定
IMAGES_STORE = 'images'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'turorial (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
# DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
      # user-agent得为手机客户端抓包获取到的
      #"User-Agent" : "DYZB/1 CFNetwork/808.2.16 Darwin/16.3.0"
#}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'turorial.middlewares.TurorialSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'turorial.middlewares.TurorialDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
  # 'turorial.pipelines.ItcastSpiderPipeline': 300,
   #'turorial.pipelines.DouyuPipeline':200,
   # 'turorial.pipelines.TencentspiderPipeline': 100,
    #'turorial.pipelines.SunSpiderPipeline': 200
    'turorial.pipelines.DoubanspiderPipeline': 200
    # 'turorial.pipelines.IpSpiderPipeline': 200
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'


DEFAULT_REQUEST_HEADERS = {
    # user-agent得为手机客户端抓包获取到的
   # "User-Agent": "DYZB/1 CFNetwork/808.2.16 Darwin/16.3.0"
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko',
}

# 优先级随机user-agent的优先级高于随机代理
DOWNLOADER_MIDDLEWARES = {
   'turorial.middlewares.RandomUserAgentMiddleware': 100,
   'turorial.middlewares.RandomProxiesMiddleware': 200,
}

# 随机user- agent列表
USER_AGENTS = [
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
  'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko',
  'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)',
  'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0',
  'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; InfoPath.3; rv:11.0) like Gecko',
  'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11',
  'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; 360SE)',
  'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SE 2.X MetaSr 1.0; SE 2.X MetaSr 1.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)',
  'Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; en) Presto/2.8.131 Version/11.11'
]

# HTTP随机代理ip地址
PROXIES =[
    {"type": "HTTP", "ip_port": "171.35.171.205:9999", "user_pwd": ""},
    {"type": "HTTP", "ip_port": "8.208.91.118:8000", "user_pwd": ""},
    {"type": "HTTP", "ip_port": "120.83.108.251:9999", "user_pwd": ""},
    {"type": "HTTP", "ip_port": "171.35.168.40:9999", "user_pwd": ""},
    {"type": "HTTP", "ip_port": "116.209.135.114:8888", "user_pwd": ""},
]

# MONGODB配置信息
MONGODB_HOST = '127.0.0.1'
MONGODB_PORT = '27017'
MONGODB_DB_NAME = 'myMongo'
MONGODB_COLL_NAME = 'doubanTop'
COOKIES_ENABLED = False

