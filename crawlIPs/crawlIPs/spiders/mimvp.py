import scrapy
from crawlIPs.items import KuaidailiItem


class MimvpSpider(scrapy.Spider):
    name = 'mimvp'
    allowed_domains = ['proxy.mimvp.com']
    start_urls = ['http://proxy.mimvp.com/freeopen']

    def parse(self, response):
        item = KuaidailiItem()
        ips = response.xpath('table[@class="mimvp-tbl free-proxylist-tbl"]//tr')
        for ip in ips:
            item['type'] = ip.xpath('./td[4]/text()').get()
            curip = ip.xpath('./td[@data-title="IP"]/text()').get()
            port = ip.xpath('./td[2]/text()').get()
            item['ip_port'] = str(curip) + ':'+str(port)
            item['user_pwd'] = ''
        pass
