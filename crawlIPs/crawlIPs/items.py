# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class CrawlipsItem(scrapy.Item):
    # define the fields for your item here like:
    pass


class KuaidailiItem(scrapy.Item):
    # http类型
    type = scrapy.Field()
    # ip地址
    ip_port = scrapy.Field()
    # 用户名密码
    user_pwd = scrapy.Field()

